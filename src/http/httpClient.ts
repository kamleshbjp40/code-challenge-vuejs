import axios from 'axios';

/**
 * Handling for API's
 */

export const HTTP = axios.create({
  baseURL: `https://abc.com/api/`,
  headers: {
    Authorization: 'Bearer {token}'
  }
})