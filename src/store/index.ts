import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    userData: {name: '', email: ''},
    loanDetails: {},
    loanStatus: ''
  },
  mutations: {
    SAVE_USER(state, data) {
      state.userData = data
    },
    SAVE_LOAN_DETAILS(state, data) {
      state.loanDetails = data;
      state.loanStatus = 'Repaid'
    }
  },
  actions: {
    SAVE_USER_DATA(context, data) {
      return new Promise((resolve,reject)=>{
        context.commit('SAVE_USER', data)
        resolve('success')
      })
      
    },
    SAVE_LOAN_DATA(context, data) {
      return new Promise((resolve,reject)=>{
        context.commit('SAVE_LOAN_DETAILS', data)
        resolve('success')
      })
      
    }
  },
  getters: {
    GET_USER_DATA: (state) => {
      return state.userData
    },
  },
  modules: {
  }
})
