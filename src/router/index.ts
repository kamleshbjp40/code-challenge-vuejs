import Vue from 'vue'
import VueRouter, { RouteConfig } from 'vue-router'

Vue.use(VueRouter)

const routes: Array<RouteConfig> = [
  
  {
    path: '/',
    name: 'Home',
    
    component: () => import('../views/Home/Home.vue'),
    children: [
      {
        path: '/',
        name: 'Card Details',    
        component: () => import('@/components/Dashboard/components/CardDetails/CardDetails.vue'),
      },
      {
        path: '/card-details',
        name: 'Card Details',    
        component: () => import('@/components/Dashboard/components/CardDetails/CardDetails.vue'),
      }
    ]
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('../views/Register/Register.vue')
  },
  {
    path: '/loan',
    name: 'Loan',
    component: () => import('../views/Loan/Loan.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
