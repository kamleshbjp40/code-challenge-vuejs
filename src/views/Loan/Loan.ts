import { Component, Vue } from 'vue-property-decorator';
import { validationMixin } from 'vuelidate'
import { required } from 'vuelidate/lib/validators'

@Component({
    mixins: [validationMixin],
    validations: {
        totalAmount: { required },
        monthlyRepaidAmount: { required},
        loanTerms: { required},
      }
})
export default class Loan extends Vue{

    totalAmount = '';
    monthlyRepaidAmount:any = '';
    loanTerms = ''
    
     get totalAmountErrors () {
        const errors:any = []
        if (!this.$v.totalAmount.$dirty) {
          return errors
        }
        !this.$v.totalAmount.required && errors.push('Total Amount is required.')
        return errors
      }
      get loanTermsErrors () {
        const errors:any = []
        if (!this.$v.loanTerms.$dirty) { 
          return errors 
        }
        !this.$v.loanTerms.required && errors.push('Loan terms is required.')
        return errors
      }
     get repaidAmountErrors () {
        const errors:any = []
        if (!this.$v.monthlyRepaidAmount.$dirty) { 
          return errors
        }
        !this.$v.monthlyRepaidAmount.required && errors.push('Repaid Amount is required')
        return errors
      }

      get curentLoanStatus(){
          return this.$store.state.loanStatus;
      }


    submit () {
        this.$v.$touch();
        if(!this.$v.$invalid){
            this.$store.dispatch('SAVE_LOAN_DATA', {totalAmount: this.totalAmount, repaidAmount: this.monthlyRepaidAmount, loanTerms: this.loanTerms})
            .then(()=>{
                this.clear()
            })
        }
      }
      clear () {
        this.$v.$reset()
        this.loanTerms = ''
        this.totalAmount = ''
        this.monthlyRepaidAmount = ''
      }
}
