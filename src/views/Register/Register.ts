import { Component, Vue } from 'vue-property-decorator';
import { validationMixin } from 'vuelidate'
import { required, maxLength, email } from 'vuelidate/lib/validators'

@Component({
    mixins: [validationMixin],
    validations: {
        name: { required, maxLength: maxLength(10) },
        email: { required, email },
      }
})
export default class Register extends Vue{

    name= '';
    email= '';
    
     get nameErrors () {
        const errors:any = []
        if (!this.$v.name.$dirty) return errors
        !this.$v.name.maxLength && errors.push('Name must be at most 10 characters long')
        !this.$v.name.required && errors.push('Name is required.')
        return errors
      }
     get emailErrors () {
        const errors:any = []
        if (!this.$v.email.$dirty) return errors
        !this.$v.email.email && errors.push('Must be valid e-mail')
        !this.$v.email.required && errors.push('E-mail is required')
        return errors
      }

      get userDetails(){
          return this.$store.state.userData;
      }


    submit () {
        this.$v.$touch();
        if(!this.$v.$invalid){
            this.$store.dispatch('SAVE_USER_DATA', {name: this.name, email: this.email})
            .then(()=>{
                this.clear()
            })
        }
      }
      clear () {
        this.$v.$reset()
        this.name = ''
        this.email = ''
      }
}
