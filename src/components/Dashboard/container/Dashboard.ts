import { Component, Vue } from 'vue-property-decorator';

import AppConst from '@/constants/AppConst'

@Component({})
export default class Dashboard extends Vue{
    
    menus = AppConst.LeftMenuList;
    activeMenu = {
        name: "Cards",
        icon: 'card.svg'
    }

}