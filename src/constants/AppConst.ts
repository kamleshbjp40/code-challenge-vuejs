export default class AppConst {
    public static LeftMenuList = [
        {
            name: "Home",
            icon: 'home.svg'
        },
        {
            name: "Cards",
            icon: 'card.svg'
        },
        {
            name: "Payments",
            icon: 'payments.svg'
        },
        {
            name: "Credit",
            icon: 'credit.svg'
        },
        {
            name: "Settings",
            icon: 'settings.svg'
        }
    ];

    public static CardActionList = [
        {
            name: "Freeze card",
            icon: 'freeze-card.svg'
        },
        {
            name: "Set spend limit",
            icon: 'set-spend-limit.svg'
        },
        {
            name: "Add to GPay",
            icon: 'gpay.svg'
        },
        {
            name: "Replace card",
            icon: 'replace-card.svg'
        },
        {
            name: "Cancel card",
            icon: 'deactivate-card.svg'
        }
    ];
}